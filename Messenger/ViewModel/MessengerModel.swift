//
//  MessengerModel.swift
//  Messenger
//
//  Created by diwka on 28/3/22.
//

import Foundation

class MessengerModel {
    var name: String?
    var lastName: String?
    var profileImage: String? 
    var statusIcon: Bool?
    var message: String?
    var storiesEvent: Bool?
    var statusLabel: Bool?
    var messageCount: String?
    var vizitDate: Bool?
    
    
    init(name:String, lastName:String, profileImage:String, statusIcon: Bool?, message: String, storiesEvent: Bool, statusLabel: Bool, messageCount: String, vizitDate: Bool) {
        self.name = name
        self.lastName = lastName
        self.profileImage = profileImage
        self.statusIcon = statusIcon
        self.message = message
        self.storiesEvent = storiesEvent
        self.statusLabel = statusLabel
        self.messageCount = messageCount
        self.vizitDate = vizitDate
    }
    init(name:String, lastName:String, profileImage:String, storiesEvent: Bool) {
        self.name = name
        self.lastName = lastName
        self.profileImage = profileImage
        self.storiesEvent = storiesEvent
    }
}
