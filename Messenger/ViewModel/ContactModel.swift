//
//  ContactModel.swift
//  Chateo-KG
//
//  Created by Samat Murzaliev on 28.03.2022.
//

import UIKit

class ContactModel {

    let name: String?
    let lastName: String?
    let profileImage: String?
    let statusIcon: String?
    let statusLabel: String?
    let storiesEvent: Bool?
    
    
    init(name:String, lastName:String, profileImage:String, statusIcon: String, statusLabel: String, storiesEvent: Bool) {
        self.name = name
        self.lastName = lastName
        self.profileImage = profileImage
        self.statusIcon = statusIcon
        self.statusLabel = statusLabel
        self.storiesEvent = storiesEvent
    }
}
