//
//  YourHistory.swift
//  Messenger
//
//  Created by diwka on 29/3/22.
//

import Foundation
import UIKit
import SnapKit
class YourHistory: UICollectionViewCell{
    let yourHistory: UIButton = {
        let view = UIButton()
        view.setImage(UIImage(named: "Vector"), for: .normal)
        view.backgroundColor = .systemGray5
        view.layer.cornerRadius = (view.frame.height + view.frame.width) / 12
        view.clipsToBounds = true
        return view
    }()
    let yourHistoryName: UILabel = {
        let view = UILabel()
        view.text = "Your Story"
        view.textAlignment = .center
        view.textColor = .black
        view.font = .systemFont(ofSize: 10, weight: .regular)
        return view
        
    }()
    override func layoutSubviews() {
        backgroundColor = .white
        addSubview(yourHistory)
        yourHistory.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.width.equalTo(56)
            make.height.equalTo(56)
            make.left.equalToSuperview().offset(24)
        }
        addSubview(yourHistoryName)
        yourHistoryName.snp.makeConstraints { make in
            make.top.equalTo(yourHistory.snp.bottom).offset(7)
            make.width.equalTo(70)
            make.left.equalToSuperview().offset(20)
        }
    }
}
