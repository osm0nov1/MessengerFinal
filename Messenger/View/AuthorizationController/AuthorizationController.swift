//
//  NumberController.swift
//  Chateo
//
//  Created by Эмир Кармышев on 28/3/22.
//

import Foundation
import UIKit
import SnapKit

class AuthorizationController: UIViewController, UITextFieldDelegate{
    
    private lazy var continueButton: UIButton = {
        let view = UIButton()
        view.setImage(UIImage(named: "ContinueButton"), for: .normal)
        view.backgroundColor = .white
        view.addTarget(self, action: #selector(continueClicked(view:)), for: .touchUpInside)
        return view
    }()
    
    private lazy var numberTextField: UITextField = {
        let view = UITextField()
        view.placeholder = "Phone Number"
        view.textColor = .black
        view.font = .systemFont(ofSize: 13, weight: .regular)
        view.layer.backgroundColor = UIColor(red: 0.969, green: 0.969, blue: 0.988, alpha: 1).cgColor
        view.keyboardType = .numberPad
        view.autocapitalizationType = .none
        view.indent(size: 10)
        view.layer.cornerRadius = 5
        view.delegate = self
        return view
    }()
    private lazy var countryBackFrame: UIView = {
        let view = UIView()
        view.layer.backgroundColor = UIColor(red: 0.969, green: 0.969, blue: 0.988, alpha: 1).cgColor
        view.layer.cornerRadius = 5
        return view
    }()
    
    private lazy var countryImage: UIImageView = {
        let view = UIImageView(image: UIImage(named: "flag"))
        view.layer.backgroundColor = UIColor(red: 0.969, green: 0.969, blue: 0.988, alpha: 1).cgColor
        return view
    }()
    
    private lazy var codeLabel: UILabel = {
        let view = UILabel()
        view.text = "+996"
        view.layer.backgroundColor = UIColor(red: 0.969, green: 0.969, blue: 0.988, alpha: 1).cgColor
        view.font = .systemFont(ofSize: 13, weight: .regular)
        view.textColor = .gray
        return view
    }()
    
    private lazy var confirmLabel: UILabel = {
        let view = UILabel()
        view.text = "Please confirm your country code and enter your phone number"
        view.frame = CGRect(x: 0, y: 0, width: 295, height: 48)
        view.textColor = .black
        view.font = .systemFont(ofSize: 14, weight: .regular)
        view.numberOfLines = 0
        view.addInterlineSpacing(spacingValue: 8.0)
        view.textAlignment = .center
        return view
    }()
    
    private lazy var enterLabel: UILabel = {
        let view = UILabel()
        view.text = "Enter Your Phone Number"
        view.frame = CGRect(x: 0, y: 0, width: 295, height: 30)
        view.textColor = .black
        view.font = .systemFont(ofSize: 24, weight: .bold)
        view.textAlignment = .center
        return view
    }()
    private lazy var topbarView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    private lazy var backButton: UIButton = {
        let view = UIButton()
        view.setImage(UIImage(named: "Vector-8"), for: .normal)
        view.backgroundColor = .white
        view.addTarget(self, action: #selector(backClicked(view:)), for: .touchUpInside)
        return view
    }()
    
    private lazy var screenFrame: UIView = {
        let view = UIView()
        
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        view.backgroundColor = .white
        
        view.addSubview(screenFrame)
        screenFrame.snp.makeConstraints { make in
            make.height.width.equalTo(self.view.frame.width)
            make.center.equalToSuperview()
        }
        
        view.addSubview(topbarView)
        topbarView.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top)
            make.left.equalTo(view.safeArea.left)
            make.right.equalTo(view.safeArea.right)
            make.height.equalTo(60)
        }
        screenFrame.addSubview(enterLabel)
        enterLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview()
            make.left.equalToSuperview().offset(40)
            make.right.equalToSuperview().offset(-40)
        }
        screenFrame.addSubview(confirmLabel)
        confirmLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(enterLabel.snp.bottom).offset(8)
            make.left.equalToSuperview().offset(40)
            make.right.equalToSuperview().offset(-40)
        }
        
        screenFrame.addSubview(numberTextField)
        numberTextField.snp.makeConstraints{ make in
            make.centerX.equalToSuperview()
            make.top.equalTo(confirmLabel.snp.bottom).offset(48)
            make.left.equalToSuperview().offset(106)
            make.right.equalToSuperview().offset(-34)
            make.width.equalTo(245)
            make.height.equalTo(36)
        }
        
        
        screenFrame.addSubview(countryBackFrame)
        countryBackFrame.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(confirmLabel.snp.bottom).offset(48)
            make.left.equalToSuperview().offset(34)
            make.right.equalTo(numberTextField.snp.left).offset(-8)
            make.width.equalTo(90)
            make.height.equalTo(36)
        }
        countryBackFrame.addSubview(countryImage)
        countryImage.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(2)
            make.top.equalToSuperview().offset(9)
            make.bottom.equalToSuperview().offset(-9)
            make.width.height.equalTo(18)
            
        }
        countryBackFrame.addSubview(codeLabel)
        codeLabel.snp.makeConstraints { make in
            make.right.equalToSuperview().offset(-2)
            make.left.equalTo(countryImage.snp.right).offset(9)
            make.top.equalToSuperview().offset(6)
            make.bottom.equalToSuperview().offset(-6)
            make.height.equalTo(24)
            make.width.equalTo(26)
        }
        
        view.addSubview(continueButton)
        continueButton.snp.makeConstraints{  make in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-54)
            //            make.right.equalToSuperview().offset(-24)
            //            make.left.equalToSuperview().offset(24)
            make.width.equalTo(327)
        }
        topbarView.addSubview(backButton)
        backButton.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(20)
            make.left.equalToSuperview().offset(24)
        }
        
        setupKeyboard()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func backClicked(view: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func continueClicked(view: UIButton) {
        self.navigationController?.pushViewController(EnterCodeController(), animated: true)
    }
    
    
    private func setupKeyboard() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func keyBoardWillShow(notification: NSNotification) {
        openKeyBoardEvent(keyBoardHeight: (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height)
    }
    
    @objc func keyBoardWillHide(notification: NSNotification) {
        hideKeyBoardEvent(keyBoardHeight: (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height)
    }
    
    func openKeyBoardEvent(keyBoardHeight: CGFloat) {
            UIView.animate(withDuration: 1) {
                self.continueButton.transform = CGAffineTransform(translationX: 0, y: (keyBoardHeight * -1) + 32)
            }
        }
        
        func hideKeyBoardEvent(keyBoardHeight: CGFloat) {
            UIView.animate(withDuration: 1) {
                self.continueButton.transform = CGAffineTransform(translationX: 0, y: 0)
            }
        }

        
        func format(with mask: String, phone: String) -> String {
            let numbers = phone.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
            var result = ""
            var index = numbers.startIndex
            for ch in mask where index < numbers.endIndex {
                if ch == "X" {
                    result.append(numbers[index])
                    index = numbers.index(after: index)
                    
                } else {
                    result.append(ch)
                }
            }
            return result
        }
        
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            guard let text = textField.text else { return false }
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            textField.text = format(with: "(XXX) XXX-XXX", phone: newString)
            return false
        }
        
        func textFieldDidBeginEditing(_ textField: UITextField) {
            whenBeginEditing()
        }
        func whenBeginEditing() {
            if self.numberTextField.isEditing == false {
                codeLabel.textColor = .gray
            }else  if self.numberTextField.isEditing == true{
                codeLabel.textColor = .black
            }
            
        }
}
extension UITextField {
    func indent(size:CGFloat) {
        self.leftView = UIView(frame: CGRect(x: self.frame.minX, y: self.frame.minY, width: size, height: self.frame.height))
        self.leftViewMode = .always
    }
}
 extension UILabel {

    func addInterlineSpacing(spacingValue: CGFloat = 2) {

        guard let textString = text else { return }

        let attributedString = NSMutableAttributedString(string: textString)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = spacingValue
        attributedString.addAttribute(
            .paragraphStyle,
            value: paragraphStyle,
            range: NSRange(location: 0, length: attributedString.length
        ))
        attributedText = attributedString
    }

}


