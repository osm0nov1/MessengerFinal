//
//  TermsController.swift
//  Chateo
//
//  Created by Эмир Кармышев on 29/3/22.
//

import Foundation
import UIKit
import SnapKit

class TermsController: UIViewController {
    
    private lazy var pizzaRecipe: UILabel = {
        let view = UILabel()
        view.text = "Ингредиенты: Тесто для пиццы: 1 штука, Сыр моцарелла: 250 г, Оливковое масло: 2 столовые ложки, Сырокопченая колбаса: 200 г, Перец чили: 1 штука, Помидоры в собственном соку: 1 банка, Орегано : 1 чайная ложка, Сушеный базилик: 1 чайная ложка, Чеснок: 1 зубчик, Сахар: 1 чайная ложка, Соль: по вкусу, Молотый черный перец: по вкусу. "
        view.textColor = .black
        view.numberOfLines = 50
        view.backgroundColor = .systemBlue
        view.textAlignment = .center
        return view
    }()
    private lazy var  psLabel: UILabel = {
        let view = UILabel()
        view.text = "Ps: Лучшая пицца та, которую заказал Эльдар <3"
        view.textColor = .black
        view.backgroundColor = .systemBlue
        view.textAlignment = .center
        view.numberOfLines = 2
        return view
    }()
    private lazy var backButton: UIButton = {
        let view = UIButton()
        view.setImage(UIImage(named: "back"), for: .normal)
        view.backgroundColor = .systemBlue
        view.addTarget(self, action: #selector(backClicked(view:)), for: .touchUpInside)
        return view
    }()
    
    override func viewDidLoad() {
        view.backgroundColor = .white
        view.addSubview(pizzaRecipe)
        pizzaRecipe.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.edges.equalToSuperview()
        }
        view.addSubview(psLabel)
        psLabel.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-100)
            make.centerX.equalToSuperview()
        }
        view.addSubview(backButton)
        backButton.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(50)
            make.left.equalToSuperview().offset(24)
        }
    }
    @objc func backClicked(view: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
