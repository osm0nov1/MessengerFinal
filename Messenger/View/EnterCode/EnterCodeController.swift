//
//  CodeController.swift
//  Chateo
//
//  Created by Эмир Кармышев on 28/3/22.
//

import UIKit
import SnapKit

class EnterCodeController: UIViewController, UITextFieldDelegate {
    
    var correctCode = "1111"
    
    private lazy var resendButton: UIButton = {
        let view = UIButton()
        view.setTitle("Resend Code", for: .normal)
        view.setTitleColor(.blue, for: .normal)
        return view
    }()
    
    let alert = AlertService.shared
    
    public lazy var codeTextField: FourDigitTextField = {
        let view = FourDigitTextField()
        view.defaultCharacter = "•"
        view.configure()
        view.didEnterLastDigit = { [weak self] code in
            print(code)
//            self?.codeVerification()
//        }
            if view.text! == "1111" {
                self?.navigationController?.pushViewController(ProfileController(), animated: true)
            } else {
                guard let alert = self?.alert.failMessage(with: code) else { return }
                self?.present(alert, animated: true)
            }
             }
        return view
    }()
    
    private lazy var infoSmsLabel: UILabel = {
        let view = UILabel()
        view.text = "We have sent you an SMS with the code to your number"
        view.frame = CGRect(x: 0, y: 0, width: 295, height: 48)
        view.textColor = .black
        view.font = .systemFont(ofSize: 14, weight: .regular)
        view.numberOfLines = 2
        view.addInterlineSpacing(spacingValue: 8.0)
        view.textAlignment = .center
        return view
    }()
    
    private lazy var enterCodeLabel: UILabel = {
        let view = UILabel()
        view.text = "Enter Code"
        view.frame = CGRect(x: 0, y: 0, width: 295, height: 30)
        view.textColor = .black
        view.font = .systemFont(ofSize: 24, weight: .bold)
        view.textAlignment = .center
        return view
    }()
    
    private lazy var topbarView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    private lazy var backButton: UIButton = {
        let view = UIButton()
        view.setImage(UIImage(named: "Vector-8"), for: .normal)
        view.backgroundColor = .white
        view.addTarget(self, action: #selector(backClicked(view:)), for: .touchUpInside)
        return view
    }()
    private lazy var screenFrame: UIView = {
        let view = UIView()
        
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        codeTextField.delegate = self
        
        
        view.backgroundColor = .white
        
//
        view.addSubview(topbarView)
        topbarView.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top)
            make.left.equalTo(view.safeArea.left)
            make.right.equalTo(view.safeArea.right)
            make.height.equalTo(60)
        }
        view.addSubview(screenFrame)
        screenFrame.snp.makeConstraints { make in
            make.height.width.equalTo(self.view.frame.width)
            make.center.equalToSuperview()
        }
        screenFrame.addSubview(enterCodeLabel)
        enterCodeLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview()
            make.left.equalToSuperview().offset(40)
            make.right.equalToSuperview().offset(-40)
        }
        screenFrame.addSubview(infoSmsLabel)
        infoSmsLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(enterCodeLabel.snp.bottom).offset(8)
            make.left.equalToSuperview().offset(40)
            make.right.equalToSuperview().offset(-40)
        }
        screenFrame.addSubview(codeTextField)
        codeTextField.snp.makeConstraints{  make in
            make.centerX.equalToSuperview()
            make.top.equalTo(infoSmsLabel.snp.bottom).offset(48)
            make.left.equalToSuperview().offset(40)
            make.right.equalToSuperview().offset(-40)
        }
        view.addSubview(resendButton)
        resendButton.snp.makeConstraints{  make in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-54)
            make.right.equalToSuperview().offset(-24)
            make.left.equalToSuperview().offset(24)
            make.width.equalTo(327)
        }
        topbarView.addSubview(backButton)
        backButton.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(20)
            make.left.equalToSuperview().offset(24)
        }
        setupKeyboard()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
//        if codeTextField.text == "1111" {
//            codeTextField.didEnterLastDigit = { [weak self] code in
//                print(code)
//                guard let alert = self?.alert.successMessage(with: code) else { return }
//                self?.present(alert, animated: true) }
//            return
//        } else {
//            codeTextField.didEnterLastDigit = { [weak self] code in
//                print(code)
//                guard let alert = self?.alert.failMessage(with: code) else { return }
//                self?.present(alert, animated: true) }
//            return
//        }
    }
    
    
    
    private func setupKeyboard() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func keyBoardWillShow(notification: NSNotification) {
        openKeyBoardEvent(keyBoardHeight: (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height)
    }
    
    @objc func keyBoardWillHide(notification: NSNotification) {
        hideKeyBoardEvent(keyBoardHeight: (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height)
    }
    
    func openKeyBoardEvent(keyBoardHeight: CGFloat) {
            UIView.animate(withDuration: 1) {
                self.resendButton.transform = CGAffineTransform(translationX: 0, y: (keyBoardHeight * -1) + 44)
            }
        }
        
        func hideKeyBoardEvent(keyBoardHeight: CGFloat) {
            UIView.animate(withDuration: 1) {
                self.resendButton.transform = CGAffineTransform(translationX: 0, y: 0)
            }
        }
    
    @objc func backClicked(view: UIButton) {
            self.navigationController?.popViewController(animated: true)

    }
//    func codeVerification() {
//        if codeTextField.text == correctCode {
//            codeTextField.didEnterLastDigit = { [weak self] code in
//                print(code)
//                guard let alert = self?.alert.successMessage(with: code) else { return }
//                self?.present(alert, animated: true) }
//            return
//        } else {
//            codeTextField.didEnterLastDigit = { [weak self] code in
//                print(code)
//                guard let alert = self?.alert.failMessage(with: code) else { return }
//                self?.present(alert, animated: true) }
//            return
//        }
//    }
}


