
import SnapKit
import UIKit

class MainController: UIViewController {
    
    private lazy var startButton: UIButton = {
        let view = UIButton()
        view.setImage(UIImage(named: "StartButton"), for: .normal)
        view.addTarget(self, action: #selector(startClicked(view:)), for: .touchUpInside)
        return view
    }()
    private lazy var privacyButton: UIButton = {
        let view = UIButton()
        view.setTitle("Terms & Privacy Policy", for: .normal)
        view.setTitleColor(.black, for: .normal)
        view.addTarget(self, action: #selector(termsClicked(view:)), for: .touchUpInside)
        return view
    }()
    private lazy var infoText: UILabel = {
        let view = UILabel()
        view.text = "Connect easily with your family and friends over countries"
        view.frame = CGRect(x: 0, y: 0, width: 280, height: 90)
        view.backgroundColor = .white
        view.textColor = .black
        view.font = .systemFont(ofSize: 24, weight: .bold)
        view.numberOfLines = 0
        view.lineBreakMode = .byWordWrapping
        view.textAlignment = .center
        return view
    }()
    private lazy var illustration: UIImageView = {
        let view = UIImageView(image: UIImage(named: "Illustration"))
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(illustration)
        illustration.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(view.frame.height * 0.13)
            make.left.equalToSuperview().offset(62)
            make.right.equalToSuperview().offset(-51)
//            make.width.equalTo(262)
//            make.height.equalTo(320)
        }
        view.addSubview(infoText)
        infoText.snp.makeConstraints{ make in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(view.safeArea.bottom).offset((view.frame.height * 0.32) * -1)
            make.left.equalToSuperview().offset(47)
            make.right.equalToSuperview().offset(-48)
            make.width.equalTo(280)
            make.height.equalTo(90)
        }
        view.addSubview(startButton)
        startButton.snp.makeConstraints{  make in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-54)
            make.right.equalToSuperview().offset(-24)
            make.left.equalToSuperview().offset(24)
        }
        view.addSubview(privacyButton)
        privacyButton.snp.makeConstraints{ make in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(startButton.snp.top).offset(-18)
            make.left.equalToSuperview().offset(100)
            make.right.equalToSuperview().offset(-100)
        }
        
        
    }
    @objc func startClicked(view: UIButton) {
        navigationController?.pushViewController(AuthorizationController(), animated: true)
    }
    @objc func termsClicked(view: UIButton) {
        navigationController?.pushViewController(TermsController(), animated: true)
    }
    
}
