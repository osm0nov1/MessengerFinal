//
//  ViewController.swift
//  tababar
//
//  Created by diwka on 2/4/22.
//
import UIKit
import SnapKit

class ViewController: UITabBarController {

    var a = ContactsViewController()
    var b = ChatsController()
    var s = SettingController()
    
    override func viewDidLoad() {
        let a1 = UITabBarItem()
        a1.image = UIImage(named: "Menu-1")
        a1.accessibilityIdentifier = "ContactsViewController"
        a.tabBarItem = a1
        
        let b1 = UITabBarItem()
        b1.image = UIImage(named: "coolicon-1")
        b1.accessibilityIdentifier = "ChatsController"
        b.tabBarItem = b1
        
        let s1 = UITabBarItem()
        s1.image = UIImage(named: "coolicon-2")
        s1.accessibilityIdentifier = "SettingController"
        s.tabBarItem = s1
        
        tabBar.isTranslucent = false
        tabBar.backgroundColor = .white
        tabBar.barTintColor = .black
        tabBar.tintColor = .black
        tabBar.unselectedItemTintColor = .black
        tabBar.clipsToBounds = true
        
        
        delegate = self
        
        viewControllers = [
            a,
            b,
            s
        ]
    }
}

extension ViewController: UITabBarControllerDelegate {
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        a.tabBarItem.image = UIImage(named: "coolicon")
        b.tabBarItem.image = UIImage(named: "coolicon-1")
        s.tabBarItem.image = UIImage(named: "coolicon-2")

        if item.accessibilityIdentifier == "ContactsViewController" {
            item.image = UIImage(named: "Menu-1")
        } else if item.accessibilityIdentifier == "ChatsController" {
            item.image = UIImage(named: "Menu")
        } else if item.accessibilityIdentifier == "SettingController"{
            item.image = UIImage(named: "Menu-2")
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        return true
    }
}
