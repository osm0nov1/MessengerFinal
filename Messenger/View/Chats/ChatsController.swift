//
//  ViewController.swift
//  Messenger
//
//  Created by diwka on 27/3/22.
//

import UIKit
import SnapKit

class ChatsController: UIViewController {
    private lazy var topbarView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    private lazy var chatsLabel: UILabel = {
        let view = UILabel()
        view.text = "Chats"
        view.font = .systemFont(ofSize: 20, weight: .medium)
        view.textColor = .black
        view.textAlignment = .left
        return view
    }()
    private lazy var newMessenger: UIButton = {
        let view = UIButton()
        view.setImage(UIImage.init(named: "newMess"), for: .normal )
        return view
    }()
    private lazy var select: UIButton = {
        let view = UIButton()
        view.setImage(UIImage.init(named: "select"), for: .normal )
        return view
    }()
    private lazy var historyCollection: UICollectionView = {
        let flow = UICollectionViewFlowLayout()
        flow.scrollDirection = .horizontal
        let view = UICollectionView(frame: .zero, collectionViewLayout: flow)
        view.delegate = self
        view.dataSource = self
        view.register(HistoryCell.self, forCellWithReuseIdentifier: "HistoryCell")
        view.backgroundColor = .white
        view.isSpringLoaded = true
        view.showsHorizontalScrollIndicator = false
        view.alwaysBounceHorizontal = true
        return view
    }()
    private lazy var searchBar: UISearchBar = {
        let view = UISearchBar()
        view.placeholder = "search"
        view.searchTextField.backgroundColor = .init(hex: "#F7F7FC")
        view.searchTextField.alpha = 0.3
        view.searchBarStyle = .minimal
//        view.alpha = 0.3
        return view
    }()
    private lazy var chatsTabel: UITableView = {
        let view = UITableView()
        view.dataSource = self
        view.delegate = self
        return view
    }()
    private lazy var vector: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 0.929, green: 0.929, blue: 0.929, alpha: 1)
        return view
    }()
    private lazy var historyArray: [MessengerModel] = [
    MessengerModel(name: "Your", lastName: "History", profileImage: "YourHistory", storiesEvent: false),
    MessengerModel(name: "Emir", lastName: "Karmyshev", profileImage: "Frame 3293", storiesEvent: true),
    MessengerModel(name: "Rawit", lastName: "Osmonov", profileImage: "", storiesEvent: true),
    ]
    private lazy var chatsArray: [MessengerModel] = [
        MessengerModel(name: "Emir", lastName: "Karmyshev", profileImage: "Frame 3293", statusIcon: true, message: "Hi! How are you?", storiesEvent: true, statusLabel: true, messageCount: "Tags", vizitDate: false),
        MessengerModel(name: "Rawit", lastName: "Osmonov", profileImage: "", statusIcon: false, message: "Hi! How are you?", storiesEvent: true, statusLabel: false, messageCount: "", vizitDate: true),
        MessengerModel(name: "Eldar", lastName: "Akkozov", profileImage: "Frame 3293-1", statusIcon: true, message: "че пивка)", storiesEvent: false, statusLabel: true ,messageCount: "Tags", vizitDate: false)
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(topbarView)
        topbarView.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top)
            make.left.equalToSuperview()
            make.right.equalTo(view.safeArea.right)
            make.height.equalTo(60)
        }
        view.addSubview(chatsLabel)
        chatsLabel.snp.makeConstraints { make in
            make.top.equalTo(topbarView.snp.top).offset(10)
            make.left.equalTo(view.safeArea.left).offset(24)
            make.width.equalTo(topbarView.snp.width).dividedBy(1.1)
            make.height.equalTo(30)
        }
        view.addSubview(select)
        select.snp.makeConstraints { make in
            make.top.equalTo(topbarView.snp.top).offset(25)
            make.right.equalTo(topbarView.snp.right).offset(-24)
        }
        view.addSubview(newMessenger)
        newMessenger.snp.makeConstraints { make in
            make.top.equalTo(topbarView.snp.top).offset(20)
            make.right.equalTo(select.snp.left).offset(-12)
            make.height.equalTo(30)
        }
        view.addSubview(historyCollection)
        historyCollection.snp.makeConstraints { make in
            make.top.equalTo(topbarView.snp.bottom).offset(-15)
            make.height.equalTo(108)
            make.width.equalTo(56)
            make.left.equalTo(view.safeArea.left)
            make.right.equalTo(view.safeArea.right)
        }
        view.addSubview(vector)
        vector.snp.makeConstraints { make in
            make.top.equalTo(historyCollection.snp.bottom).offset(10)
            make.left.equalTo(view.safeArea.left)
            make.right.equalTo(view.safeArea.right)
            make.height.equalTo(1)
        }
        view.addSubview(searchBar)
        searchBar.snp.makeConstraints { make in
            make.top.equalTo(vector.snp.bottom).offset(5)
            make.left.equalTo(view.safeArea.left).offset(17)
            make.right.equalTo(view.safeArea.right).offset(-17)
        }
        view.addSubview(chatsTabel)
        chatsTabel.snp.makeConstraints { make in
            make.top.equalTo(searchBar.snp.bottom).offset(15)
            make.right.equalTo(view.safeArea.right).offset(-24)
            make.left.equalTo(view.safeArea.left).offset(24)
            make.bottom.equalTo(view.safeArea.bottom).offset(30)
        }
    }
}
extension ChatsController: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 74
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        let cell = ChatsCell()
        let model = chatsArray[index]
        cell.fill(chats: model)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return historyArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let index = indexPath.row
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HistoryCell", for: indexPath) as!  HistoryCell
        let model = historyArray[index]
        if index == 0 {
            cell.fill(history: model, yourStory: false)
        } else {
            cell.fill(history: model, yourStory: true)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 56, height: 76 )
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
}
