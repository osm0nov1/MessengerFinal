//
//  ContackController.swift
//  Messenger
//
//  Created by diwka on 2/4/22.
//

import Foundation
import UIKit
class ContactsViewController: UIViewController{
    
    private var topNavbarView: UIView = {
        let view = UIView()
//        view.backgroundColor = .lightGray
        return view
    }()
    
    let contacts: [ContactModel] = [
        ContactModel(name: "Athalia", lastName: "Putri", profileImage: "contact1", statusIcon: "statusOffline", statusLabel: "Last seen yesterday", storiesEvent: true),
        ContactModel(name: "Erlan", lastName: "Sadewa", profileImage: "contact2", statusIcon: "statusOnline", statusLabel: "Online", storiesEvent: true),
        ContactModel(name: "Midala", lastName: "Huera", profileImage: "contact3", statusIcon: "", statusLabel: "Last seen 3 hours ago", storiesEvent: true),
        ContactModel(name: "Nafisa", lastName: "Gitari", profileImage: "contact4", statusIcon: "statusOnline", statusLabel: "Online", storiesEvent: false),
        ContactModel(name: "Raki", lastName: "Devon", profileImage: "", statusIcon: "statusOnline", statusLabel: "Online", storiesEvent: true),
        ContactModel(name: "Salsabila", lastName: "Akira", profileImage: "", statusIcon: "", statusLabel: "Last seen 30 minutes ago", storiesEvent: true)]
    
    private var topNavbarLabel: UILabel = {
        let view = UILabel()
        view.text = "Contacts"
        view.font = .systemFont(ofSize: 18, weight: .semibold)
        view.textColor = .black
        
        return view
    }()
    
    private var topNabarAddButton: UIButton = {
        let view = UIButton(type: .system)
        view.setImage(UIImage(named: "AddContact"), for: .normal)
        view.tintColor = .black
        
        return view
    }()
    
    private lazy var searchField: UISearchBar = {
        let view = UISearchBar()
        view.placeholder = "Search"
        view.searchBarStyle = .minimal
        view.autocorrectionType = .no
        
        return view
    }()
    
    private lazy var contactsTable: UITableView = {
        let view = UITableView()
        view.delegate = self
        view.dataSource = self
        
        return view
    }()
    
    private lazy var bottomTabBar: UIView = {
        let view = UIView()
        
        return view
    }()
    
    override func viewDidLoad() {
        view.backgroundColor = .white
        setSubViews()
        
    }
    
    private func setSubViews() {
        
        view.addSubview(topNavbarView)
        topNavbarView.snp.makeConstraints { make in
            make.top.equalTo(self.view.safeArea.top)
            make.left.right.equalToSuperview()
            make.height.equalTo(57)
        }
        
        topNavbarView.addSubview(topNavbarLabel)
        topNavbarLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(24)
        }
        
        topNavbarView.addSubview(topNabarAddButton)
        topNabarAddButton.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-24)
        }
        
        view.addSubview(searchField)
        searchField.snp.makeConstraints { make in
            make.top.equalTo(topNavbarView.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(17)
            make.right.equalToSuperview().offset(-17)
            make.height.equalTo(36)
        }
        
        view.addSubview(contactsTable)
        contactsTable.snp.makeConstraints { make in
            make.top.equalTo(searchField.snp.bottom).offset(20)
            make.left.equalToSuperview().offset(24)
            make.right.equalToSuperview().offset(-24)
            make.bottom.equalTo(self.view.safeArea.bottom).offset(30)
        }
    }
}


extension ContactsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ContactTableCellView()
        let model = contacts[indexPath.row]
        cell.fill(contact: model)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 74
    }
    
}


