//
//  ChatViewController.swift
//  Chateo-KG
//
//  Created by Samat Murzaliev on 29.03.2022.
//

import UIKit
import SnapKit

class ChatViewController: UIViewController {
        
    let customCells: [UITableViewCell] = [FifthMessageCell(), FourthMessageCell(), ThirdMessageCell(), SeparatorCell(), SecondMessageCell(), FirstMessageCell()]
      
    private lazy var topNavBarView: UIView = {
        let view = UIView()
        
        return view
    }()
    
    private lazy var topNavBarBottomLine: UIView = {
        let view = UIView()
        view.layer.borderWidth = 1

        view.layer.borderColor = UIColor(red: 0.929, green: 0.929, blue: 0.929, alpha: 1).cgColor
        return view
    }()
    
    private lazy var topNavBackButton: UIButton = {
        let view = UIButton(type: .system)
        view.setImage(UIImage(named: "navbarBackButton"), for: .normal)
        view.tintColor = .black
        
        return view
    }()
    
    private lazy var topNavContactLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 18, weight: .medium)
        view.textColor = .black
        view.text = "Athalia Putri"
        
        return view
    }()
    
    private lazy var topNavMoreButton: UIButton = {
        let view = UIButton(type: .system)
        view.setImage(UIImage(named: "navbarMoreButton"), for: .normal)
        view.tintColor = .black
        
        return view
    }()
    
    private lazy var topSearchButton: UIButton = {
        let view = UIButton(type: .system)
        view.setImage(UIImage(named: "navbarSearchButton"), for: .normal)
        view.tintColor = .black
        
        return view
    }()
    
    private lazy var topNavAddButton: UIButton = {
        let view = UIButton(type: .system)
        view.setImage(UIImage(named: "navbarAddButton"), for: .normal)
        view.tintColor = .black
        view.isHidden = true
        
        return view
    }()
    
    private lazy var chatBackView: UITableView = {
        let view = UITableView()
        view.delegate = self
        view.dataSource = self
        view.allowsSelection = false
        view.transform = CGAffineTransform(scaleX: 1, y: -1)
        view.scrollIndicatorInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: self.view.bounds.size.width - 10)

        return view
    }()
    
       
    private lazy var bottomSendBar: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        
        return view
    }()
    
    private lazy var bottomBarAddButton: UIButton = {
        let view = UIButton(type: .system)
        view.setImage(UIImage(named: "textFieldLeadingAddIcon"), for: .normal)
        view.tintColor = .gray
        
        return view
    }()
    
    private lazy var bottomBarTextField: UITextField = {
        let view = UITextField()
        view.placeholder = "Enter your message"
        view.borderStyle = .roundedRect
        view.backgroundColor = UIColor(named: "chatViewBackColor")
        view.font = .systemFont(ofSize: 14, weight: .semibold)
        view.autocorrectionType = .no
        view.delegate = self
        
        return view
    }()
    
    private lazy var bottomBarSendButton: UIButton = {
        let view = UIButton(type: .system)
        view.setImage(UIImage(named: "textFieldSendButton"), for: .normal)
        view.tintColor = .blue
        view.isEnabled = false
        
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setSubViews()


    }
    
    private func setSubViews() {
        view.backgroundColor = .white
        
        view.addSubview(topNavBarView)
        topNavBarView.snp.makeConstraints { make in
            make.left.right.equalToSuperview()
            make.top.equalTo(self.view.safeArea.top)
            make.height.equalTo(50)
        }
        
        topNavBarView.addSubview(topNavBackButton)
        topNavBackButton.snp.makeConstraints { make in
            make.height.width.equalTo(24)
            make.left.equalToSuperview().offset(16)
            make.bottom.equalToSuperview().offset(-18)
        }
        
        topNavBarView.addSubview(topNavContactLabel)
        topNavContactLabel.snp.makeConstraints { make in
            make.left.equalTo(topNavBackButton.snp.right).offset(10)
            make.top.equalToSuperview().offset(10)
        }
        
        topNavBarView.addSubview(topNavMoreButton)
        topNavMoreButton.snp.makeConstraints { make in
            make.height.width.equalTo(24)
            make.bottom.right.equalToSuperview().offset(-16)
        }
        
        topNavBarView.addSubview(topSearchButton)
        topSearchButton.snp.makeConstraints { make in
            make.height.width.equalTo(24)
            make.bottom.equalToSuperview().offset(-16)
            make.right.equalTo(topNavMoreButton.snp.left).offset(-8)
        }
        
        topNavBarView.addSubview(topNavAddButton)
        topNavAddButton.snp.makeConstraints { make in
            make.height.width.equalTo(24)
            make.bottom.equalToSuperview().offset(-16)
            make.right.equalTo(topSearchButton.snp.left).offset(-8)
        }
        
        topNavBarView.addSubview(topNavBarBottomLine)
        topNavBarBottomLine.snp.makeConstraints { make in
            make.height.equalTo(1)
            make.left.right.bottom.equalToSuperview()
        }
        
        view.addSubview(chatBackView)
        chatBackView.snp.makeConstraints { make in
            make.left.right.equalToSuperview()
            make.top.equalTo(topNavBarView.snp.bottom)
            make.bottom.equalTo(self.view.safeArea.bottom).offset(-56)
        }
        
        view.addSubview(bottomSendBar)
        bottomSendBar.snp.makeConstraints { make in
            make.bottom.equalTo(self.view.safeArea.bottom)
            make.left.right.equalToSuperview()
            make.height.equalTo(56)
        }
        
        bottomSendBar.addSubview(bottomBarAddButton)
        bottomBarAddButton.snp.makeConstraints { make in
            make.height.width.equalTo(24)
            make.top.equalToSuperview().offset(16)
            make.left.equalToSuperview().offset(12)
        }
        
        bottomSendBar.addSubview(bottomBarTextField)
        bottomBarTextField.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.left.equalToSuperview().offset(50)
            make.right.equalToSuperview().offset(-50)
        }
        
        bottomSendBar.addSubview(bottomBarSendButton)
        bottomBarSendButton.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(16)
            make.left.equalTo(bottomBarTextField.snp.right).offset(12)
        }
    }
}

extension ChatViewController: UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        customCells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = customCells[indexPath.row]
        cell.contentView.transform = CGAffineTransform(rotationAngle: .pi)
        
        return cell
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.bottomBarSendButton.isEnabled = true
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
//        if indexPath.row == 5 {
//            return 218
//        } else if indexPath.row == 4 {
//            return 128
//        } else if indexPath.row == 3 {
//            return 64
//        } else if indexPath.row == 2 {
//          return 20
//        } else if indexPath.row == 1 {
//            return 77
//        } else if indexPath.row == 0 {
//            return 64
//        } else {
//            return 50
//        }
    }
    
}
