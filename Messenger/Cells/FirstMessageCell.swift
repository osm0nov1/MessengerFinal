//
//  FirstMessageCell.swift
//  Chateo-KG
//
//  Created by Samat Murzaliev on 31.03.2022.
//

import UIKit
import SnapKit

class FirstMessageCell: UITableViewCell {
    
        
    private lazy var messageLayer: UIImageView = {
        let view = UIImageView(image: UIImage(named: "firstChatLayer"))
        view.contentMode = .scaleAspectFit
        
        view.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowRadius = 4
        view.layer.shadowOffset = CGSize(width: 0, height: 4)

        return view
    }()
    
    private lazy var chatImage: UIImageView = {
        let view = UIImageView(image: UIImage(named: "chatImage1"))
        view.contentMode = .scaleAspectFit
        
        return view
    }()
    
    private lazy var chatMessageLabel: UILabel = {
        let view = UILabel()
        view.text = "But don’t worry cause we are all learning here"
        view.font = .systemFont(ofSize: 14, weight: .thin)
        view.textColor = .black
        
//        view.backgroundColor = .orange
        
        return view
    }()
    
    private lazy var chatTimeLabel: UILabel = {
        let view = UILabel()
        view.text = "16.46"
        view.font = .systemFont(ofSize: 10, weight: .thin)
        view.textColor = .gray
        
        return view
    }()
    
    override func layoutSubviews() {
        backgroundColor = UIColor(named: "chatViewBackColor")
        
        addSubview(messageLayer)
        messageLayer.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-42)
        }
                     
        messageLayer.addSubview(chatImage)
        chatImage.snp.makeConstraints { make in
            make.top.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
        }
        
        messageLayer.addSubview(chatMessageLabel)
        chatMessageLabel.snp.makeConstraints { make in
            make.top.equalTo(chatImage.snp.bottom).offset(10)
            make.left.equalTo(chatImage.snp.left).offset(9)
        }
        
        messageLayer.addSubview(chatTimeLabel)
        chatTimeLabel.snp.makeConstraints { make in
            make.top.equalTo(chatMessageLabel.snp.bottom).offset(4)
            make.left.equalTo(chatImage.snp.left).offset(9)
            make.bottom.equalToSuperview()
        }
        
        transform = CGAffineTransform(scaleX: 1, y: -1)
    }
}
