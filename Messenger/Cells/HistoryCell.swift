//
//  HistoryCell.swift
//  Messenger
//
//  Created by diwka on 28/3/22.
//

import Foundation
import SnapKit
import UIKit
class HistoryCell: UICollectionViewCell {
    private lazy var yourHistory: UIButton = {
        let view = UIButton()
        view.setImage(UIImage(named: "YourHistory"), for: .normal)
        view.layer.cornerRadius = (view.frame.height + view.frame.width) / 12
        view.clipsToBounds = true
        return view
    }()
    private lazy var yourHistoryName: UILabel = {
        let view = UILabel()
        view.text = "Your Story"
        view.textAlignment = .center
        view.textColor = .black
        view.font = .systemFont(ofSize: 10, weight: .light)
        return view
    }()
    private lazy var contactImage: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        view.backgroundColor = UIColor(red: 0.086, green: 0.435, blue: 0.965, alpha: 1)
        view.layer.cornerRadius = 16
        return view
    }()
    private lazy var contactImageBackground: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 0.086, green: 0.435, blue: 0.965, alpha: 1)
        view.layer.cornerRadius = 16
        return view
    }()
    private lazy var storyStroke: UIImageView = {
        let view = UIImageView(image: UIImage(named: "Story"))
        return view
    }()
    private lazy var nameLabel: UILabel = {
        let view = UILabel()
        view.textAlignment = .center
        view.textColor = .black
        view.font = .systemFont(ofSize: 10, weight: .light)
        return view
    }()
    private lazy var LastLabel: UILabel = {
        let view = UILabel()
        view.textAlignment = .center
        view.textColor = .black
        view.font = .systemFont(ofSize: 16, weight: .regular)
        return view
    }()
    private lazy var contactInitials: UILabel = {
        let view = UILabel()
        view.text = "RD"
        view.font = .systemFont(ofSize: 14, weight: .bold)
        view.textAlignment = .center
        view.textColor = .white
        view.textColor = .white
        view.backgroundColor = .systemBlue
        view.layer.cornerRadius = 13
        view.clipsToBounds = true
        return view
    }()
    private lazy var profileFrame: UIView = {
        let view = UIView()
        return view
    }()
    private  lazy var yourStoryStroke: UIImageView = {
        let view = UIImageView(image: UIImage(named: "Story-1"))
        return view
    }()
    
    override func layoutSubviews() {
        addSubview(profileFrame)
        profileFrame.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.height.equalTo(56)
            make.left.equalToSuperview().offset(24)
        }
        profileFrame.addSubview(yourStoryStroke)
        yourStoryStroke.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.height.width.equalTo(56)
        }
        yourStoryStroke.addSubview(yourHistory)
        yourHistory.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        profileFrame.addSubview(yourHistoryName)
        yourHistoryName.snp.makeConstraints { make in
            make.top.equalTo(yourStoryStroke.snp.bottom).offset(4)
            make.left.equalToSuperview()
            make.width.equalTo(56)
            make.height.equalTo(16)
        }
        profileFrame.addSubview(storyStroke)
        storyStroke.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.height.width.equalTo(56)
        }
        storyStroke.addSubview(contactImage)
        contactImage.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        storyStroke.addSubview(nameLabel)
        nameLabel.snp.makeConstraints { make in
            make.top.equalTo(storyStroke.snp.bottom).offset(4)
            make.left.equalToSuperview()
            make.width.equalTo(56)
            make.height.equalTo(16)
        }
        storyStroke.addSubview(contactInitials)
        contactInitials.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.equalTo(48)
            make.height.equalTo(48)
        }
    }
    func fill(history: MessengerModel, yourStory: Bool) {
        self.nameLabel.text = "\(history.name!) \(history.lastName!)"
        self.storyStroke.isHidden = history.storiesEvent! ? false : true
        self.contactImage = UIImageView(image: UIImage(named: history.profileImage!))
        self.contactInitials.isHidden = history.profileImage!.count>0 ? true : false
        self.contactInitials.text = "\(String(describing: history.name!.first!))\(String(describing: history.lastName!.first!))"
        self.yourHistory.isHidden = yourStory ? true: false
        self.yourHistoryName.isHidden = yourStory ? true: false
        self.yourStoryStroke.isHidden = yourStory ? true: false
    }
}
