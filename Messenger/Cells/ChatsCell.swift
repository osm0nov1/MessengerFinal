//
//  ChatsCell.swift
//  Messenger
//
//  Created by diwka on 29/3/22.
//

import Foundation
import UIKit
import SnapKit

class ChatsCell: UITableViewCell {
    
    private lazy var profileFrame: UIView = {
        let view = UIView()
        return view
    }()
    private lazy  var storyStroke: UIImageView = {
        let view = UIImageView(image: UIImage(named: "Story"))
        return view
    }()
    private lazy var contactImage: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        view.backgroundColor = UIColor(red: 0.086, green: 0.435, blue: 0.965, alpha: 1)
        view.layer.cornerRadius = 16
        return view
    }()
    private  lazy var contactImageBackground: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 0.086, green: 0.435, blue: 0.965, alpha: 1)
        view.layer.cornerRadius = 16
        return view
    }()
    private  lazy var contactInitials: UILabel = {
        let view = UILabel()
        view.text = "RD"
        view.font = .systemFont(ofSize: 14, weight: .bold)
        view.textAlignment = .center
        view.textColor = .white
        return view
    }()
    private lazy var statusImage: UIImageView = {
        let view = UIImageView(image: UIImage(named: "Status"))
        view.contentMode = .scaleAspectFit
        return view
    }()
    private lazy var nameLabel: UILabel = {
        let view = UILabel()
        view.text = "Athalia Putri"
        view.font = .systemFont(ofSize: 14, weight: .medium)
        view.textColor = .black
        return view
    }()
    private lazy var message: UILabel = {
        let view = UILabel()
        view.text = "Last seen yesterday"
        view.font = .systemFont(ofSize: 12, weight: .regular)
        view.textColor = .lightGray
        return view
    }()
    private lazy var customChatCell: UIView = {
        let view = UIView()
        return view
    }()
    private lazy var vizitTime: UILabel = {
        let view = UILabel()
        view.text = "Today"
        view.font = .systemFont(ofSize: 10, weight: .light)
        view.textColor = UIColor(red: 0.643, green: 0.643, blue: 0.643, alpha: 1)
        return view 
    }()
    private lazy var vizitDate: UILabel = {
        let view = UILabel()
        view.text = "17/6"
        view.font = .systemFont(ofSize: 10, weight: .light)
        view.textColor = UIColor(red: 0.643, green: 0.643, blue: 0.643, alpha: 1)
        return view
    }()
    private lazy var messageCount: UILabel = {
        let view = UILabel()
        view.text = "1"
        view.font = .systemFont(ofSize: 10, weight: .light)
        view.textColor = .black
        view.textAlignment = .center
        return view
    }()
    private lazy var messageCountImage: UIImageView = {
        let view = UIImageView(image: UIImage(named: ""))
        view.contentMode = .scaleAspectFit
        return view
    }()
    override func layoutSubviews() {
        addSubview(profileFrame)
        profileFrame.snp.makeConstraints { make in
            make.top.left.equalToSuperview()
            make.height.width.equalTo(56)
        }
        profileFrame.addSubview(storyStroke)
        storyStroke.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        profileFrame.addSubview(contactImageBackground)
        contactImageBackground.snp.makeConstraints { make in
            make.height.width.equalTo(48)
            make.center.equalToSuperview()
        }
        profileFrame.addSubview(contactImage)
        contactImage.snp.makeConstraints { make in
            make.height.width.equalTo(48)
            make.center.equalToSuperview()
        }
        profileFrame.addSubview(statusImage)
        statusImage.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(2)
            make.right.equalToSuperview().offset(-2)
        }
        profileFrame.addSubview(contactInitials)
        contactInitials.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        addSubview(nameLabel)
        nameLabel.snp.makeConstraints {make in
            make.left.equalTo(profileFrame.snp.right).offset(12)
            make.top.equalToSuperview().offset(4)
        }
        addSubview(message)
        message.snp.makeConstraints {make in
            make.left.equalTo(profileFrame.snp.right).offset(12)
            make.top.equalTo(nameLabel.snp.bottom).offset(8)
        }
        addSubview(vizitTime)
        vizitTime.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(16)
            make.width.equalTo(29)
        }
        addSubview(vizitDate)
        vizitDate.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(16)
            make.width.equalTo(29)
        }
        addSubview(messageCountImage)
        messageCountImage.snp.makeConstraints { make in
            make.top.equalTo(vizitTime.snp.bottom).offset(10)
            make.right.equalToSuperview()
            make.height.equalTo(20)
            make.width.equalTo(22)
        }
    }
    func fill(chats: MessengerModel){
        self.nameLabel.text = "\(String(describing: chats.name!)) \(String(describing: chats.lastName!))"
        self.message.text = chats.message
        self.storyStroke.isHidden = chats.storiesEvent! ? false : true
        self.statusImage.isHidden = chats.statusIcon! ? false : true
        self.contactImage = UIImageView(image: UIImage(named: chats.profileImage!))
        self.contactInitials.isHidden = chats.profileImage!.count>0 ? true : false
        self.contactInitials.text = "\(String(describing: chats.name!.first!))\(String(describing: chats.lastName!.first!))"
        self.vizitTime.isHidden = chats.statusLabel! ? false : true
        self.vizitDate.isHidden = chats.vizitDate! ? false : true
        self.messageCountImage = UIImageView(image: UIImage(named: chats.messageCount!))
    }
}

