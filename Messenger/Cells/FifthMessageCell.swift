//
//  FifthMessageCell.swift
//  Chateo-KG
//
//  Created by Samat Murzaliev on 02.04.2022.
//

import UIKit
import SnapKit

class FifthMessageCell: UITableViewCell {
    
    private lazy var imageLayer: UIImageView = {
        let view = UIImageView(image: UIImage(named: "fiveChatLayer"))
        view.contentMode = .scaleAspectFit
        
        return view
    }()
    
    private lazy var messageLabel: UILabel = {
        let view = UILabel()
        view.text = "But don’t worry cause we are all learning here"
        view.font = .systemFont(ofSize: 14, weight: .light)
        view.textColor = .black
        
        return view
    }()
    
    private lazy var chatTimeLabel: UILabel = {
        let view = UILabel()
        view.text = "16.46 · Read"
        view.font = .systemFont(ofSize: 10, weight: .thin)
        view.textColor = .gray
        
        return view
    }()
    
    
    override func layoutSubviews() {
        
        backgroundColor = UIColor(named: "chatViewBackColor")
        
        addSubview(imageLayer)
        imageLayer.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-42)
        }
        
        imageLayer.addSubview(messageLabel)
        messageLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.left.equalToSuperview().offset(15)
        }
        
        imageLayer.addSubview(chatTimeLabel)
        chatTimeLabel.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-13)
            make.left.equalTo(messageLabel.snp.left)
        }
        
        transform = CGAffineTransform(scaleX: 1, y: -1)
    }
}
