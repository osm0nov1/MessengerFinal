//
//  FourthMessageCell.swift
//  Chateo-KG
//
//  Created by Samat Murzaliev on 02.04.2022.
//

import UIKit
import SnapKit

class FourthMessageCell: UITableViewCell {
    
    private lazy var imageLayer: UIImageView = {
        let view = UIImageView(image: UIImage(named: "fourChatLayer"))
        view.contentMode = .scaleAspectFit
        
        return view
    }()
    
    private lazy var chatTimeLabel: UILabel = {
        let view = UILabel()
        view.text = "16.46 · Read"
        view.font = .systemFont(ofSize: 10, weight: .thin)
        view.textColor = .white
        
        return view
    }()
    
    
    override func layoutSubviews() {
        
        backgroundColor = UIColor(named: "chatViewBackColor")
        
        addSubview(imageLayer)
        imageLayer.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.left.equalToSuperview().offset(200)
            make.right.equalToSuperview().offset(-16)
        }
        
        imageLayer.addSubview(chatTimeLabel)
        chatTimeLabel.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-13)
            make.right.equalToSuperview().offset(-19)
        }
        
        transform = CGAffineTransform(scaleX: 1, y: -1)
    }
}
