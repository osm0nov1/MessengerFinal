//
//  SecondMessageCell.swift
//  Chateo-KG
//
//  Created by Samat Murzaliev on 31.03.2022.
//

import UIKit
import SnapKit

class SecondMessageCell: UITableViewCell {
    
    private lazy var imageLayer: UIImageView = {
        let view = UIImageView(image: UIImage(named: "secondChatLayer"))
        view.contentMode = .scaleAspectFit
        
        return view
    }()
    
    private lazy var replyContactNameLabel: UILabel = {
        let view = UILabel()
        view.text = "You"
        view.font = .systemFont(ofSize: 10, weight: .regular)
        view.textColor = .blue
        
        return view
    }()
    
    private lazy var replyOriginalMessage: UILabel = {
        let view = UILabel()
        view.text = "But don’t worry cause we are all learning here"
        view.font = .systemFont(ofSize: 14, weight: .light)
        view.textColor = .black
        
        return view
    }()
    
    private lazy var messageLabel: UILabel = {
        let view = UILabel()
        view.text = "But don’t worry cause we are all learning here"
        view.font = .systemFont(ofSize: 14, weight: .light)
        view.textColor = .black
        
        return view
    }()
    
    private lazy var chatTimeLabel: UILabel = {
        let view = UILabel()
        view.text = "16.46"
        view.font = .systemFont(ofSize: 10, weight: .thin)
        view.textColor = .gray
        
        return view
    }()
    
    
    override func layoutSubviews() {
        
        backgroundColor = UIColor(named: "chatViewBackColor")
        
        addSubview(imageLayer)
        imageLayer.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-22)
        }
        
        imageLayer.addSubview(replyContactNameLabel)
        replyContactNameLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(18)
            make.left.equalToSuperview().offset(24)
        }
        
        imageLayer.addSubview(replyOriginalMessage)
        replyOriginalMessage.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(38)
            make.left.equalTo(replyContactNameLabel.snp.left)
        }
        
        imageLayer.addSubview(messageLabel)
        messageLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.bottom.equalToSuperview().offset(-30)
        }
        
        imageLayer.addSubview(chatTimeLabel)
        chatTimeLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(102)
            make.left.equalTo(messageLabel.snp.left)
        }
        
        transform = CGAffineTransform(scaleX: 1, y: -1)
    }
}
