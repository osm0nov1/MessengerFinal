//
//  SeparatorCell.swift
//  Chateo-KG
//
//  Created by Samat Murzaliev on 02.04.2022.
//

import UIKit
import SnapKit

class SeparatorCell: UITableViewCell {
    
    private lazy var imageLayer: UIImageView = {
        let view = UIImageView(image: UIImage(named: "dividerChat"))
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    override func layoutSubviews() {
        
        addSubview(imageLayer)
        imageLayer.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        transform = CGAffineTransform(scaleX: 1, y: -1)

    }
}
