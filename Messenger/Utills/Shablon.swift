////
////  Shablon.swift
////  Messenger
////
////  Created by diwka on 30/3/22.
////
//
//import Foundation
//import SnapKit
//import UIKit
//class Shablon: UITableViewCell {
//    
//    private var profileFrame: UIView = {
//        let view = UIView()
//        
//        return view
//    }()
//    
//    private var storyStroke: UIImageView = {
//        let view = UIImageView(image: UIImage(named: "storyStroke"))
//        
//        return view
//    }()
//    
//    private var contactImage: UIImageView = {
////        let view = UIImageView(image: UIImage(named: "contact1"))
//        let view = UIImageView()
//        view.contentMode = .scaleAspectFit
//        view.backgroundColor = UIColor(red: 0.086, green: 0.435, blue: 0.965, alpha: 1)
//        view.layer.cornerRadius = 16
//        
//        return view
//    }()
//    
//    private var contactImageBackground: UIView = {
//        let view = UIView()
//        view.backgroundColor = UIColor(red: 0.086, green: 0.435, blue: 0.965, alpha: 1)
//        view.layer.cornerRadius = 16
//        
//        return view
//    }()
//    
//    private var contactInitials: UILabel = {
//        let view = UILabel()
//        view.text = "RD"
//        view.font = .systemFont(ofSize: 14, weight: .bold)
//        view.textAlignment = .center
//        view.textColor = .white
//        
//        return view
//    }()
//    
//    private var statusImage: UIImageView = {
//        let view = UIImageView(image: UIImage(named: "statusOffline"))
//        view.contentMode = .scaleAspectFit
//        return view
//    }()
//    
//    private var nameLabel: UILabel = {
//        let view = UILabel()
//        view.text = "Athalia Putri"
//        view.font = .systemFont(ofSize: 14, weight: .medium)
//        view.textColor = .black
//        
//        return view
//    }()
//    
//    private var statusLabel: UILabel = {
//        let view = UILabel()
//        view.text = "Last seen yesterday"
//        view.font = .systemFont(ofSize: 12, weight: .regular)
//        view.textColor = .lightGray
//        
//        return view
//    }()
//    
//    private var dividerImage: UIImageView = {
//        let view = UIImageView(image: UIImage(named: "divider"))
//        view.contentMode = .scaleAspectFit
//        return view
//    }()
//    
//    private var customChatCell: UIView = {
//        let view = UIView()
//        
//        return view
//    }()
//    
//    override func layoutSubviews() {
//        addSubview(profileFrame)
//        profileFrame.snp.makeConstraints { make in
//            make.top.left.equalToSuperview()
//            make.height.width.equalTo(56)
//        }
//        
//        profileFrame.addSubview(storyStroke)
//        storyStroke.snp.makeConstraints { make in
//            make.edges.equalToSuperview()
//        }
//        
//        profileFrame.addSubview(contactImageBackground)
//        contactImageBackground.snp.makeConstraints { make in
//            make.height.width.equalTo(48)
//            make.center.equalToSuperview()
//        }
//        
//        profileFrame.addSubview(contactImage)
//        contactImage.snp.makeConstraints { make in
//            make.height.width.equalTo(48)
//            make.center.equalToSuperview()
//        }
//        
//        profileFrame.addSubview(statusImage)
//        statusImage.snp.makeConstraints { make in
//            make.top.equalToSuperview().offset(2)
//            make.right.equalToSuperview().offset(-2)
//        }
//        profileFrame.addSubview(contactInitials)
////        contactInitials.isHidden = true
//        contactInitials.snp.makeConstraints { make in
//            make.center.equalToSuperview()
//        }
//        
//        addSubview(nameLabel)
//        nameLabel.snp.makeConstraints {make in
//            make.left.equalTo(profileFrame.snp.right).offset(12)
//            make.top.equalToSuperview().offset(4)
//        }
//        
//        addSubview(statusLabel)
//        statusLabel.snp.makeConstraints {make in
//            make.left.equalTo(profileFrame.snp.right).offset(12)
//            make.top.equalTo(nameLabel.snp.bottom).offset(8)
//        }
//        
//        addSubview(dividerImage)
//        dividerImage.snp.makeConstraints { make in
//            make.centerX.equalToSuperview()
//            make.bottom.equalToSuperview().offset(-8)
//        }
//    }
//    
//    func fill(contact: MessengerModel) {
//        self.nameLabel.text = "\(contact.name!) \(contact.lastName!)"
//        self.statusLabel.text = contact.statusLabel
//        self.storyStroke.isHidden = contact.storiesEvent! ? false : true
////        self.statusImage.image
//        self.contactImage = UIImageView(image: UIImage(named: contact.profileImage!))
//        self.statusImage = UIImageView(image: UIImage(named: contact.statusIcon!))
//        self.contactInitials.isHidden = contact.profileImage!.count>0 ? true : false
//        self.contactInitials.text = "\(String(describing: contact.name!.first!))\(contact.lastName!.first!)"
//    }
//}
