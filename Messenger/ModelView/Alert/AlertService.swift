import Foundation
import UIKit

class AlertService {
    
    private init() {}
    static let shared = AlertService()
    
    var correctCode = "1111"
    
    func failMessage(with code: String) -> UIAlertController {
        let alert = UIAlertController(title: "Fail", message: "You have entered incorrect code: \(code)", preferredStyle: .alert)
        let action = UIAlertAction(title: "Try again", style: .cancel)
        alert.addAction(action)
        return alert
    }
}
